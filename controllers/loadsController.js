const createError = require('http-errors');
const asyncHandler = require('express-async-handler');
const Load = require('../models/load');
const Truck = require('../models/truck');

const {
  addLoadSchema,
} = require('../validationSchemas/load');
const {
  sprinter,
  smallStraight,
  largeStraight,
} = require('../constants/truckTypes');

const loadStates = require('../constants/loadStates');

module.exports.getLoads = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const {
    role,
  } = req.user;
  let loads;
  if (role === 'SHIPPER') {
    loads = await Load.find({
      created_by: userId,
    });
  } else {
    loads = await Load.find({
      assigned_to: userId,
    });
  }
  res.json({
    loads,
  });
});

module.exports.getLoad = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const loadId = req.params.loadId;

  const load = await Load.findById({
    _id: loadId,
  });
  if (!load) {
    throw createError(400, `Load with id ${loadId} not found`);
  }
  if (load.created_by.toString() !== userId) {
    throw createError(401, 'You cannot see not your loads');
  }
  res.send(load);
});

module.exports.addLoad = asyncHandler(async (req, res) => {
  const {
    error,
  } = await addLoadSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }
  const userId = req.user._id;
  const load = new Load({
    created_by: userId,
    ...req.body,
  });
  await load.save();
  res.json({
    message: 'Load created successfully',
  });
});

module.exports.postLoad = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const loadId = req.params.loadId;

  const load = await Load.findOne({
    _id: loadId,
  });
  if (!load) {
    throw createError(400, `Load with id ${loadId} not found`);
  }
  if (load.created_by.toString() !== userId) {
    throw createError(401, 'You can post only your own loads');
  }
  if (load.status !== 'NEW') {
    throw createError(401, 'You can post only the loads with status NEW');
  }

  await Load.findByIdAndUpdate({
    _id: loadId,
  }, {
    status: 'POSTED',
    state: 'En route to Pick Up',
    $push: {
      logs: {
        time: (new Date()).toISOString(),
        message: 'Load was posted',
      },
    },
  });

  const candidate = await findTruck(load);

  if (!candidate) {
    await Load.findByIdAndUpdate({
      _id: loadId,
    }, {
      status: 'NEW',
      $push: {
        logs: {
          time: (new Date()).toISOString(),
          message: 'Load status changed to NEW. No trucks found.',
        },
      },
    });
    throw createError(400, 'No trucks found');
  }

  await Truck.findByIdAndUpdate({
    _id: candidate._id,
  }, {
    status: 'OL',
  });
  await Load.findByIdAndUpdate({
    _id: loadId,
  }, {
    status: 'ASSIGNED',
    state: 'En route to Pick Up',
    assigned_to: candidate.assigned_to,
    $push: {
      logs: {
        time: (new Date()).toISOString(),
        message: 'Load was assigned to driver',
      },
    },
  });
  res.json({
    message: 'Load posted successfully',
    driver_found: true,
  });
});

module.exports.updateLoadState = asyncHandler(async (req, res) => {
  const userId = req.user._id;

  const {
    _id: loadId,
    state: currentState,
    assigned_to: assignedTo,
  } = await Load.findOne({
    assigned_to: userId,
  });

  const currentStateIndex = loadStates.findIndex((el) => el === currentState);
  if (currentStateIndex === loadStates.length - 1) {
    throw createError(400, 'The Load already in its final state');
  }
  const nextState = loadStates[currentStateIndex + 1];

  await Load.findByIdAndUpdate({
    _id: loadId,
  }, {
    state: nextState,
    $push: {
      logs: {
        time: (new Date()).toISOString(),
        message: `Load state changed to '${nextState}'`,
      },
    },
  });

  const isShipped = (currentStateIndex === loadStates.length - 2);

  if (isShipped) {
    await Load.findByIdAndUpdate({
      _id: loadId,
    }, {
      status: 'SHIPPED',
    });

    await Truck.findOneAndUpdate({
      assigned_to: assignedTo,
    }, {
      status: 'IS',
    });
  }
  res.json({
    message: `Load state changed to '${nextState}'`,
  });
});

module.exports.updateLoadInfo = asyncHandler(async (req, res) => {
  const {
    error,
  } = await addLoadSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }

  const newLoadInfo = req.body;
  const userId = req.user._id;
  const loadId = req.params.loadId;
  const load = await Load.findOne({
    _id: loadId,
  });
  if (!load) {
    throw createError(400, `Load with id ${loadId} not found`);
  }
  const {
    created_by: createdBy,
    status,
  } = load;
  if (createdBy.toString() !== userId) {
    throw createError(401, 'You can update only your own loads');
  }
  if (status !== 'NEW') {
    throw createError(401, 'You can update only the loads with status NEW');
  }
  await Load.findOneAndUpdate({
    _id: loadId,
  }, newLoadInfo);
  res.json({
    message: 'Load details changed successfully',
  });
});

module.exports.deleteLoad = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const loadId = req.params.loadId;
  const load = await Load.findOne({
    _id: loadId,
  });
  if (!load) {
    throw createError(400, `Load with id ${loadId} not found`);
  }
  const {
    created_by: createdBy,
    status,
  } = load;
  if (createdBy.toString() !== userId) {
    throw createError(401, 'You can delete only your own loads');
  }
  if (status !== 'NEW') {
    throw createError(401, 'You can delete only the loads with status NEW');
  }
  await Load.deleteOne({
    _id: loadId,
  });
  res.json({
    message: 'Load deleted successfully',
  });
});

module.exports.getActiveLoad = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const load = await Load.findOne({
    assigned_to: userId,
  });
  res.json(load);
});

module.exports.getShippingInfo = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const loadId = req.params.loadId;
  const load = await Load.findById({
    _id: loadId,
  });
  if (!load) {
    throw createError(400, `Load with id ${loadId} not found or not active`);
  }
  const {
    created_by: createdBy,
    status,
  } = load;
  if (createdBy.toString() !== userId) {
    throw createError(401, 'You can view only your own loads');
  }
  if (status === 'NEW' || status === 'POSTED') {
    throw createError(400, 'Load is inactive');
  }
  const truck = await Truck.findOne({
    assigned_to: load.assigned_to,
  });

  res.json({
    load,
    truck,
  });
});

const findTruck = asyncHandler(async (load) => {
  const trucks = await Truck.find({
    status: 'IS',
    assigned_to: {
      $ne: null,
    },
  });
  if (trucks.length === 0) {
    throw createError(400, 'No trucks found');
  }
  const candidateTrucks = trucks.filter((truck) => {
    let dimensions;
    switch (truck.type) {
      case 'SPRINTER': {
        dimensions = sprinter;
        break;
      }
      case 'SMALL STRAIGHT': {
        dimensions = smallStraight;
        break;
      }
      case 'LARGE STRAIGHT': {
        dimensions = largeStraight;
        break;
      }
    }
    if (dimensions && dimensions.width >= load.dimensions.width &&
      dimensions.height >= load.dimensions.height &&
      dimensions.length >= load.dimensions.length &&
      dimensions.payload >= load.payload) {
      return truck;
    }
  });

  // let index;
  if (candidateTrucks.length !== 0) {
    // For random truck pick
    // index = Math.floor(Math.random() * (candidateTrucks.length));
    return candidateTrucks[0];
  }
});
