/* eslint-disable camelcase */
const createError = require('http-errors');
const asyncHandler = require('express-async-handler');
const Truck = require('../models/truck');
const {
  addTruckSchema,
} = require('../validationSchemas/truck');

module.exports.getTrucks = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const result = await Truck.find({created_by: userId});
  const trucks = result.map((truck) => {
    return {
      _id: truck._id,
      created_by: truck.created_by,
      assigned_to: truck.assigned_to,
      type: truck.type,
      status: truck.status,
      created_date: truck.created_date,
    };
  });
  res.json({
    trucks,
  });
});

module.exports.addTruck = asyncHandler(async (req, res) => {
  const {
    error,
  } = await addTruckSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }
  const userId = req.user._id;
  const {
    type,
  } = req.body;
  const truck = new Truck({
    created_by: userId,
    type,
  });
  await truck.save();
  res.json({
    message: 'Truck created successfully',
  });
});

module.exports.getTruckInfo = asyncHandler(async (req, res) => {
  const truckId = req.params.truckId;
  const truck = await Truck.findById(truckId);
  if (!truck) {
    throw createError(400, `Truck with id ${truckId} not found`);
  }
  const {
    _id,
    created_by,
    assigned_to,
    type,
    status,
    created_date,
  } = truck;
  res.json({
    _id,
    created_by,
    assigned_to,
    type,
    status,
    created_date,
  });
});

module.exports.assignTruck = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.truckId;
  const assigned = await Truck.find({assigned_to: userId});
  if (assigned.length > 0) {
    throw createError(400, 'You can assign only one truck');
  }
  const {created_by: truckOwner} = await Truck.findOne({_id: truckId});
  if (truckOwner.toString() !== userId) {
    throw createError(400, 'You can assign only your own trucks');
  }
  const truck = await Truck.findOneAndUpdate({
    _id: truckId,
  }, {
    assigned_to: userId,
    status: 'IS',
  });
  if (!truck) {
    throw createError(400, `Truck with id ${truckId} not found`);
  }
  res.json({
    message: 'Truck assigned successfully',
  });
});

module.exports.updateTruckInfo = asyncHandler(async (req, res) => {
  const newTruckInfo = req.body;
  const userId = req.user._id;
  const truckId = req.params.truckId;
  const truck = await Truck.findOne({
    _id: truckId,
  });
  if (!truck) {
    throw createError(400, `Truck with id ${truckId} not found`);
  }
  const {
    assigned_to: assignedTo,
  } = truck;
  if (assignedTo && assignedTo === userId) {
    throw createError(401, 'You cannot update info of assigned to you truck');
  }
  await Truck.findOneAndUpdate({
    _id: truckId,
  }, newTruckInfo);
  res.json({
    message: 'Truck details changed successfully',
  });
});

module.exports.deleteTruck = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.truckId;
  const truck = await Truck.findOne({
    _id: truckId,
  });
  if (!truck) {
    throw createError(400, `Truck with id ${truckId} not found`);
  }
  const {
    assigned_to: assignedTo,
  } = truck;
  if (assignedTo && assignedTo === userId) {
    throw createError(401, 'You cannot delete assigned to you truck');
  }
  await Truck.deleteOne({
    _id: truckId,
  });
  res.json({
    message: 'Truck deleted successfully',
  });
});
