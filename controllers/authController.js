const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const createError = require('http-errors');
const asyncHandler = require('express-async-handler');
const config = require('config');

const authConfig = config.get('auth');

const {
  registerSchema,
  loginSchema
} = require('../validationSchemas/auth');
const {
  generatePassword
} = require('../helpers/generatePassword');
const {
  sendEmail
} = require('../helpers/mailer');

const hashPassword = async (password) => {
  return await bcrypt.hash(password, 10);
};

const validatePassword = async (plainPassword, hashedPassword) => {
  return await bcrypt.compare(plainPassword, hashedPassword);
};

const register = asyncHandler(async (req, res) => {
  const {
    error
  } = await registerSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }
  const {
    email,
    password,
    role,
  } = req.body;

  const candidate = await User.findOne({
    email,
  });
  if (!candidate) {
    const hashedPassword = await hashPassword(password);
    const user = new User({
      email,
      password: hashedPassword,
      role,
    });
    await user.save();
    res.json({
      message: 'Profile created successfully',
    });
  } else {
    throw createError(409, 'This email is assigned to another user');
  }
});

const login = asyncHandler(async (req, res) => {
  const {
    error
  } = await loginSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }

  const {
    email,
    password,
  } = req.body;

  const user = await User.findOne({
    email,
  }).exec();
  if (!user) {
    throw createError(400, 'No user with such username and password found');
  }

  const validPassword = await validatePassword(password, user.password);
  if (!validPassword) throw createError(400, 'Password is not correct');

  res.json({
    jwt_token: jwt.sign(JSON.stringify(user), authConfig.secret),
  });
});


const resetPassword = asyncHandler(async (req, res) => {
  const {
    email
  } = req.body;

  const user = await User.findOne({
    email
  });
  if (!user) {
    throw createError(404, `User with email ${email} not found`);
  }

  const newPassword = generatePassword();
  const hashedPassword = await hashPassword(newPassword);

  await User.findOneAndUpdate({
    email,
  }, {
    password: hashedPassword,
  });

  await sendEmail(newPassword, email);

  res.json({
    message: 'New password sent to your email address'
  });

});

module.exports = {
  register,
  login,
  hashPassword,
  validatePassword,
  resetPassword,
};