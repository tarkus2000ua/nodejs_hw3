/* eslint-disable camelcase */
const User = require('../models/user');
const createError = require('http-errors');
const {
  hashPassword,
  validatePassword,
} = require('./authController');
const asyncHandler = require('express-async-handler');

const {
  changePasswordSchema,
} = require('../validationSchemas/user');


module.exports.getUserProfile = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    throw createError(401, 'Unauthenticated user');
  }
  const user = await User.findById(userId);
  if (user) {
    const {
      _id,
      email,
      created_date,
    } = user;
    res.json({
      user: {
        _id,
        email,
        created_date,
      },
    });
  } else {
    throw createError(400, `User with id ${userId} not found`);
  }
});

module.exports.deleteUserProfile = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    throw createError(401, 'Unauthenticated user');
  }
  await User.deleteOne({
    '_id': userId,
  });
  res.json({
    message: 'Profile deleted successfully',
  });
});

module.exports.changeUserPassword = asyncHandler(async (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    throw createError(401, 'Unauthenticated user');
  }

  const {
    error,
  } = await changePasswordSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }

  const {
    newPassword,
    oldPassword,
  } = req.body;

  const user = await User.findById(userId);
  const validPassword = await validatePassword(oldPassword, user.password);
  if (!validPassword) {
    throw createError(400, `Wrong old password provided`);
  }

  if (newPassword === oldPassword) {
    throw createError(400, 'New password should be different from the old one');
  }

  const hashedPassword = await hashPassword(newPassword);
  await User.findByIdAndUpdate({
    '_id': userId,
  }, {
    password: hashedPassword,
  });
  res.json({
    message: 'Password changed successfully',
  });
});
