const express = require('express');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose');
const fs = require('fs');
const morgan = require('morgan');
const path = require('path');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./docs/index');
const errorHandlerMiddleware = require('./middleware/errorHandlerMiddleware');
const config = require('config');

const dbConfig = config.get('db');
const srvConfig = config.get('server');

const {
  port,
} = srvConfig;
const {
  user,
  password,
  host,
  databaseName,
} = dbConfig;
const routers = require('./routers');

app.use(
    cors({
      origin: '*',
      allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type',
        'Accept', 'Authorization',
      ],
    }),
);

mongoose.connect(`mongodb+srv://${user}:${password}@${host}/${databaseName}?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

const accessLogStream = fs.createWriteStream(path.join(__dirname,
    'logs', 'access.log'), {
  flags: 'a',
});


app.use(express.json());
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// setup the logger
app.use(morgan('combined', {
  stream: accessLogStream,
}));
routers(app);

app.use(errorHandlerMiddleware);

const PORT = process.env.PORT || port || 8000;

app.listen(PORT, () => {
  console.log(`Server started on port ${port}`);
});
