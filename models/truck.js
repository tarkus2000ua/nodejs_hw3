const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TruckSchema = new Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'user',
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  status: {
    type: String,
    required: true,
    enum: ['IS', 'OL', 'OS'],
    default: 'OS',
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
}, {
  timestamps: {
    createdAt: 'created_date',
    updatedAt: 'updated_date',
  },
});

const Truck = mongoose.model('truck', TruckSchema);

module.exports = Truck;
