const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const loadStates = require('../constants/loadStates');

const LoadSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'user',
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  status: {
    type: String,
    required: true,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: loadStates,
  },
  logs: {
    type: Array,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
}, {
  timestamps: {
    createdAt: 'created_date',
    updatedAt: 'updated_date',
  },
});

const Load = mongoose.model('load', LoadSchema);

module.exports = Load;
