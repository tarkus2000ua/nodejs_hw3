const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: ['DRIVER', 'SHIPPER'],
  },
}, {
  timestamps: {
    createdAt: 'created_date',
    updatedAt: 'updated_date',
  },
});

const User = mongoose.model('user', UserSchema);

module.exports = User;
