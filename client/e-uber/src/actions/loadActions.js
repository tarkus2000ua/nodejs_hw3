import axios from "axios";
import {
  LOAD_LIST_REQUEST,
  LOAD_LIST_SUCCESS,
  LOAD_LIST_FAIL,
  LOAD_ADD_REQUEST,
  LOAD_ADD_SUCCESS,
  LOAD_ADD_FAIL,
  LOAD_UPDATE_REQUEST,
  LOAD_UPDATE_SUCCESS,
  LOAD_UPDATE_FAIL,
  LOAD_DELETE_REQUEST,
  LOAD_DELETE_SUCCESS,
  LOAD_DELETE_FAIL,
  LOAD_POST_REQUEST,
  LOAD_POST_SUCCESS,
  LOAD_POST_FAIL,
  SHIPPING_INFO_REQUEST,
  SHIPPING_INFO_SUCCESS,
  SHIPPING_INFO_FAIL
} from '../constants/loadConstants';

export const getLoadList = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: LOAD_LIST_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.get('/api/loads', config)

    dispatch({
      type: LOAD_LIST_SUCCESS,
      payload: data
    })

  } catch (error) {
    dispatch({
      type: LOAD_LIST_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const addLoad = (load) => async (dispatch, getState) => {
  try {
    dispatch({
      type: LOAD_ADD_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.post('/api/loads', load, config)

    dispatch({
      type: LOAD_ADD_SUCCESS,
      payload: data
    })

  } catch (error) {
    dispatch({
      type: LOAD_ADD_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const updateLoad = (loadId, load) => async (dispatch, getState) => {
  try {
    dispatch({
      type: LOAD_UPDATE_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.put(`/api/loads/${loadId}`, load, config)

    dispatch({
      type: LOAD_UPDATE_SUCCESS,
      payload: data
    })

  } catch (error) {
    dispatch({
      type: LOAD_UPDATE_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const deleteLoad = (load) => async (dispatch, getState) => {
  try {
    dispatch({
      type: LOAD_DELETE_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.delete(`/api/loads/${load._id}`, config)

    dispatch({
      type: LOAD_DELETE_SUCCESS,
      payload: data
    })

    dispatch(getLoadList());

  } catch (error) {
    dispatch({
      type: LOAD_DELETE_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const postLoad = (load) => async (dispatch, getState) => {
  try {
    dispatch({
      type: LOAD_POST_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.post(`/api/loads/${load._id}/post`, {}, config)

    dispatch({
      type: LOAD_POST_SUCCESS,
      payload: data
    })

    dispatch(getLoadList());

  } catch (error) {
    dispatch({
      type: LOAD_POST_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const getShippingInfo = (loadId) => async (dispatch, getState) => {
  try {
    dispatch({
      type: SHIPPING_INFO_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.get(`/api/loads/${loadId}/shipping_info`, config)

    dispatch({
      type: SHIPPING_INFO_SUCCESS,
      payload: data
    })

  } catch (error) {
    dispatch({
      type: SHIPPING_INFO_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}