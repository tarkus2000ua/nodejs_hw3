import axios from "axios";
import {
  TRUCK_LIST_REQUEST,
  TRUCK_LIST_SUCCESS,
  TRUCK_LIST_FAIL,
  TRUCK_ADD_REQUEST,
  TRUCK_ADD_SUCCESS,
  TRUCK_ADD_FAIL,
  TRUCK_UPDATE_REQUEST,
  TRUCK_UPDATE_SUCCESS,
  TRUCK_UPDATE_FAIL,
  TRUCK_DELETE_REQUEST,
  TRUCK_DELETE_SUCCESS,
  TRUCK_DELETE_FAIL,
  TRUCK_ASSIGN_REQUEST,
  TRUCK_ASSIGN_SUCCESS,
  TRUCK_ASSIGN_FAIL,
  ACTIVE_LOAD_REQUEST,
  ACTIVE_LOAD_SUCCESS,
  ACTIVE_LOAD_FAIL,
  CHANGE_LOAD_STATE_REQUEST,
  CHANGE_LOAD_STATE_SUCCESS,
  CHANGE_LOAD_STATE_FAIL
} from '../constants/truckConstants';

export const getTruckList = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: TRUCK_LIST_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.get('/api/trucks', config)

    dispatch({
      type: TRUCK_LIST_SUCCESS,
      payload: data
    })

  } catch (error) {
    dispatch({
      type: TRUCK_LIST_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const addTruck = (truckType) => async (dispatch, getState) => {
  try {
    dispatch({
      type: TRUCK_ADD_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.post('/api/trucks', {type: truckType}, config)

    dispatch({
      type: TRUCK_ADD_SUCCESS,
      payload: data
    })

  } catch (error) {
    dispatch({
      type: TRUCK_ADD_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const updateTruck = (truckId, truckType) => async (dispatch, getState) => {
  try {
    dispatch({
      type: TRUCK_UPDATE_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.put(`/api/trucks/${truckId}`, {type: truckType}, config)

    dispatch({
      type: TRUCK_UPDATE_SUCCESS,
      payload: data
    })

  } catch (error) {
    dispatch({
      type: TRUCK_UPDATE_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const deleteTruck = (truck) => async (dispatch, getState) => {
  try {
    dispatch({
      type: TRUCK_DELETE_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.delete(`/api/trucks/${truck._id}`, config)

    dispatch({
      type: TRUCK_DELETE_SUCCESS,
      payload: data
    })

    dispatch(getTruckList());

  } catch (error) {
    dispatch({
      type: TRUCK_DELETE_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const assignTruck = (truck) => async (dispatch, getState) => {
  try {
    dispatch({
      type: TRUCK_ASSIGN_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.post(`/api/trucks/${truck._id}/assign`, {}, config)

    dispatch({
      type: TRUCK_ASSIGN_SUCCESS,
      payload: data
    })

    dispatch(getTruckList());

  } catch (error) {
    dispatch({
      type: TRUCK_ASSIGN_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const getActiveLoad = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: ACTIVE_LOAD_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.get('/api/loads/active', config)

    dispatch({
      type: ACTIVE_LOAD_SUCCESS,
      payload: data
    })

  } catch (error) {
    dispatch({
      type: ACTIVE_LOAD_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}

export const changeLoadState = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: CHANGE_LOAD_STATE_REQUEST
    })

    const {
      user: {
        userInfo
      }
    } = getState();

    const config = {
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${userInfo.jwt_token}`
      }
    }

    const {
      data
    } = await axios.patch(`/api/loads/active/state`, {}, config)

    dispatch({
      type: CHANGE_LOAD_STATE_SUCCESS,
      payload: data
    })

    dispatch(getActiveLoad());

  } catch (error) {
    dispatch({
      type: CHANGE_LOAD_STATE_FAIL,
      payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
}