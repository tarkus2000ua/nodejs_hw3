import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import HomeScreen from './screens/HomeScreen';
import LoadListScreen from './screens/LoadListScreen';
import LoadEditScreen from './screens/LoadEditScreen';
import TruckListScreen from './screens/TruckListScreen';
import TruckEditScreen from './screens/TruckEditScreen';
import LoadInfoScreen from './screens/LoadInfoScreen';
import ShippingInfoScreen from './screens/ShippingInfoScreen';
import ProfileScreen from './screens/ProfileScreen';
import './App.css';

const App = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <Container>
          <main className="py-3">
            <Route path='/login' component={LoginScreen} />
            <Route path='/register' component={RegisterScreen} />
            <Route path='/profile' component={ProfileScreen} />
            <Route exact path='/loads' component={LoadListScreen} />
            <Route path='/loads/add' component={LoadEditScreen} />       
            <Route path='/loads/info' component={ShippingInfoScreen} />       
            <Route exact path='/trucks' component={TruckListScreen} />
            <Route path='/trucks/add' component={TruckEditScreen} />
            <Route path='/trucks/load/active' component={LoadInfoScreen} />
            <Route exact path='/' component={HomeScreen} />
          </main>
        </Container>
        <Footer />
      </Router>
    </div>
  );
};

export default App;
