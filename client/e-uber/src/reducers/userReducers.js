import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAIL,
  USER_LOGOUT,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAIL,
  USER_DETAILS_REQUEST,
  USER_DETAILS_SUCCESS,
  USER_DETAILS_FAIL,
  USER_DETAILS_RESET,
  USER_CHANGE_PASSWORD_REQUEST,
  USER_CHANGE_PASSWORD_SUCCESS,
  USER_CHANGE_PASSWORD_FAIL,
  USER_UPDATE_PROFILE_RESET,
  USER_DELETE_PROFILE_REQUEST,
  USER_DELETE_PROFILE_SUCCESS,
  USER_DELETE_PROFILE_FAIL
} from '../constants/userConstants';


export const userReducer = (state = {userInfo:{}, userProfile:{}}, action) => {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
      return { ...state, loading: true }
    case USER_LOGIN_SUCCESS:
      return { ...state, loading: false, userInfo: action.payload }
    case USER_LOGIN_FAIL:
      return { ...state, loading: false, error: action.payload }
    case USER_LOGOUT:
      return {}

    case USER_REGISTER_REQUEST:
      return { ...state, loading: true }
    case USER_REGISTER_SUCCESS:
      return { ...state, loading: false, userInfo: action.payload }
    case USER_REGISTER_FAIL:
      return { ...state, loading: false, error: action.payload }

    case USER_DETAILS_REQUEST:
      return { ...state, loading: true }
    case USER_DETAILS_SUCCESS:
      return { ...state, loading: false, userProfile: action.payload }
    case USER_DETAILS_FAIL:
      return { ...state, loading: false, error: action.payload }
    case USER_DETAILS_RESET:
      return { user: {} }

    case USER_CHANGE_PASSWORD_REQUEST:
      return { ...state, loading: true }
    case USER_CHANGE_PASSWORD_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case USER_CHANGE_PASSWORD_FAIL:
      return { ...state, loading: false, error: action.payload }

    case USER_DELETE_PROFILE_REQUEST:
      return { ...state, loading: true }
    case USER_DELETE_PROFILE_SUCCESS:
      return { loading: false, message: action.payload.message }
    case USER_DELETE_PROFILE_FAIL:
      return { ...state, loading: false, error: action.payload }
    
    case 'RESET_MESSAGES':
        return {...state, error: null, message: null}
    case USER_UPDATE_PROFILE_RESET:
      return {}
    default:
      return state
  }
}