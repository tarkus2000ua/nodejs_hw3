import {
  LOAD_LIST_REQUEST,
  LOAD_LIST_SUCCESS,
  LOAD_LIST_FAIL,
  LOAD_ADD_REQUEST,
  LOAD_ADD_SUCCESS,
  LOAD_ADD_FAIL,
  LOAD_UPDATE_REQUEST,
  LOAD_UPDATE_SUCCESS,
  LOAD_UPDATE_FAIL,
  LOAD_DELETE_REQUEST,
  LOAD_DELETE_SUCCESS,
  LOAD_DELETE_FAIL,
  LOAD_POST_REQUEST,
  LOAD_POST_SUCCESS,
  LOAD_POST_FAIL,
  SHIPPING_INFO_REQUEST,
  SHIPPING_INFO_SUCCESS,
  SHIPPING_INFO_FAIL,
} from '../constants/loadConstants';


export const loadReducer = (state = {}, action) => {
  switch (action.type) {
    case LOAD_LIST_REQUEST:
      return { ...state, loading: true}
    case LOAD_LIST_SUCCESS:
      return { ...state, loading: false, loads: action.payload.loads }
    case LOAD_LIST_FAIL:
      return { ...state, loading: false, error: action.payload }

    case LOAD_ADD_REQUEST:
      return { ...state, loading: true }
    case LOAD_ADD_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case LOAD_ADD_FAIL:
      return { ...state, loading: false, error: action.payload }

    case LOAD_UPDATE_REQUEST:
      return { ...state, loading: true }
    case LOAD_UPDATE_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case LOAD_UPDATE_FAIL:
      return { ...state, loading: false, error: action.payload }

    case LOAD_DELETE_REQUEST:
      return { ...state, loading: true }
    case LOAD_DELETE_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case LOAD_DELETE_FAIL:
      return { ...state, loading: false, error: action.payload }

    case LOAD_POST_REQUEST:
      return { ...state, loading: true }
    case LOAD_POST_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case LOAD_POST_FAIL:
      return { ...state, loading: false, error: action.payload }

    case SHIPPING_INFO_REQUEST:
      return { ...state, loading: true }
    case SHIPPING_INFO_SUCCESS:
      return { ...state, loading: false, shippingInfo: action.payload }
    case SHIPPING_INFO_FAIL:
      return { ...state, loading: false, error: action.payload }
    
    case 'RESET_MESSAGES':
        return {...state, error: null, message: null}
    default:
      return state
  }
}