import {
  TRUCK_LIST_REQUEST,
  TRUCK_LIST_SUCCESS,
  TRUCK_LIST_FAIL,
  TRUCK_ADD_REQUEST,
  TRUCK_ADD_SUCCESS,
  TRUCK_ADD_FAIL,
  TRUCK_UPDATE_REQUEST,
  TRUCK_UPDATE_SUCCESS,
  TRUCK_UPDATE_FAIL,
  TRUCK_DELETE_REQUEST,
  TRUCK_DELETE_SUCCESS,
  TRUCK_DELETE_FAIL,
  TRUCK_ASSIGN_REQUEST,
  TRUCK_ASSIGN_SUCCESS,
  TRUCK_ASSIGN_FAIL,
  ACTIVE_LOAD_REQUEST,
  ACTIVE_LOAD_SUCCESS,
  ACTIVE_LOAD_FAIL,
  CHANGE_LOAD_STATE_REQUEST,
  CHANGE_LOAD_STATE_SUCCESS,
  CHANGE_LOAD_STATE_FAIL
} from '../constants/truckConstants';


export const truckReducer = (state = {}, action) => {
  switch (action.type) {
    case TRUCK_LIST_REQUEST:
      return { ...state, loading: true }
    case TRUCK_LIST_SUCCESS:
      return { ...state, loading: false, trucks: action.payload.trucks }
    case TRUCK_LIST_FAIL:
      return { ...state, loading: false, error: action.payload }

    case TRUCK_ADD_REQUEST:
      return { ...state, loading: true }
    case TRUCK_ADD_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case TRUCK_ADD_FAIL:
      return { ...state, loading: false, error: action.payload }
    
    case TRUCK_UPDATE_REQUEST:
      return { ...state, loading: true }
    case TRUCK_UPDATE_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case TRUCK_UPDATE_FAIL:
      return { ...state, loading: false, error: action.payload }
    
    case TRUCK_DELETE_REQUEST:
      return { ...state, loading: true }
    case TRUCK_DELETE_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case TRUCK_DELETE_FAIL:
      return { ...state, loading: false, error: action.payload }

    case TRUCK_ASSIGN_REQUEST:
      return { ...state, loading: true }
    case TRUCK_ASSIGN_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case TRUCK_ASSIGN_FAIL:
      return { ...state, loading: false, error: action.payload }

    case ACTIVE_LOAD_REQUEST:
        return { ...state, loading: true }
    case ACTIVE_LOAD_SUCCESS:
        return { ...state, loading: false, activeLoad: action.payload }
    case ACTIVE_LOAD_FAIL:
        return { ...state, loading: false, error: action.payload }

    case CHANGE_LOAD_STATE_REQUEST:
      return { ...state, loading: true }
    case CHANGE_LOAD_STATE_SUCCESS:
      return { ...state, loading: false, message: action.payload.message }
    case CHANGE_LOAD_STATE_FAIL:
      return { ...state, loading: false, error: action.payload }
    
    case 'RESET_MESSAGES':
      return {...state, error: null, message: null}
    default:
      return state
  }
}