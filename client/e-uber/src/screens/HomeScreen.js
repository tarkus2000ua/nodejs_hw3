import React from 'react';
import { Link } from 'react-router-dom';
import { Col, Row, Button } from 'react-bootstrap';

const HomeScreen = () => {
  return (
    <Row className="mt-5">
      <Col md={6} className="mx-auto">
        <Row>
          <Col>
            <Link to="/trucks">
              <Button variant="info" size="lg">
                TRUCKS
              </Button>
            </Link>
          </Col>
          <Col>
            <Link to="/loads">
              <Button variant="info" size="lg">
                LOADS
              </Button>
            </Link>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default HomeScreen;
