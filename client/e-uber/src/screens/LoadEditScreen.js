import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { Form, Button} from 'react-bootstrap';
import Message from '../components/Message/Message';
import Loader from '../components/Loader/Loader';
import FormContainer from '../components/FormContainer/FormContainer';
import { addLoad, updateLoad } from '../actions/loadActions';

const LoadEditScreen = () => {
  const [name, setName] = useState('');
  const [pickupAddress, setPickupAddress] = useState('');
  const [deliveryAddress, setDeliveryAddress] = useState('');
  const [payload, setPayload] = useState(0);
  const [width, setWidth] = useState(0);
  const [length, setLength] = useState(0);
  const [height, setHeight] = useState(0);

  let location = useLocation();
  const load = location.state?.load; 
  const dispatch = useDispatch();
    
  useEffect(()=>{
    if (load) {
      setName(load.name);
      setPickupAddress(load.pickup_address);
      setDeliveryAddress(load.delivery_address);
      setPayload(load.payload);
      setWidth(load.dimensions.width);
      setLength(load.dimensions.length);
      setHeight(load.dimensions.height);
    }
     return () => {
       dispatch({type: 'RESET_MESSAGES'})
     }
  },[load, dispatch])


  const loadState = useSelector((state) => state.loads);
  const { loading, error, message } = loadState;

  const submitHandler = (e) => {
    e.preventDefault();
    if (load) {
      // DISPATCH UPDATE TRUCK
      dispatch(updateLoad(load._id, {name, pickup_address: pickupAddress, delivery_address: deliveryAddress, payload, dimensions: {width, length, height}}));
    } else {
    // DISPATCH ADD TRUCK
    dispatch(addLoad({
      name,pickup_address:pickupAddress,delivery_address:deliveryAddress,payload,dimensions: {width,length,height}
    }));
  }
  };
  return (
    <FormContainer>
      <h1>{load ? 'Update load' : 'Add Load'}</h1>
      {error && <Message variant="danger">{error}</Message>}
      {message && <Message variant="info">{message}</Message>}
      {loading && <Loader />}
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter load name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="pickupAddress">
          <Form.Label>Pickup Address</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter pickup address"
            value={pickupAddress}
            onChange={(e) => setPickupAddress(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="deliveryAddress">
          <Form.Label>Delivery Address</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter delivery address"
            value={deliveryAddress}
            onChange={(e) => setDeliveryAddress(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="payload">
          <Form.Label>Payload</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter payload"
            value={payload}
            onChange={(e) => setPayload(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="width">
          <Form.Label>Width</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter load width"
            value={width}
            onChange={(e) => setWidth(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="length">
          <Form.Label>Length</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter load length"
            value={length}
            onChange={(e) => setLength(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="height">
          <Form.Label>Height</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter load height"
            value={height}
            onChange={(e) => setHeight(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Button type="submit" variant="primary">
          Save
        </Button>
      </Form>
    </FormContainer>
  );
};

export default LoadEditScreen;
