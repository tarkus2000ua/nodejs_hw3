import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Table, Button, Row, Col } from 'react-bootstrap';
import Message from '../components/Message/Message';
import Loader from '../components/Loader/Loader';
import { getLoadList } from '../actions/loadActions';
import LoadItem from '../components/LoadItem/LoadItem';

const LoadList = ({history}) => {
  const loadState = useSelector((state) => state.loads);
  const { loading, error, loads, message } = loadState;
  const dispatch = useDispatch();

  const userState = useSelector((state) => state.user);

  useEffect(() => {
    if (!userState.userInfo) {
      history.push('/login');
    } else {
      dispatch(getLoadList());
    }
  }, [dispatch, history, userState.userInfo]);
  return (
    <>
      <h1>Load List</h1>
      {error && <Message variant="danger">{error}</Message>}
      {message && <Message variant="success">{message}</Message>}
      {loading && <Loader />}

      <Link to='/loads/add'>
        <Button variant="primary" className="my-3">Add load</Button>
      </Link>
      <Row>
        <Col>
          <Table striped bordered hover size="sm">
            <thead>
              <tr style={{'textAlign':'center'}}>
                <th>#</th>
                <th>Load name</th>
                <th>Status</th>
                <th>Post</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {loads &&
                loads.map((load, index) => (
                  <LoadItem load={load} index={index} key={load._id} />
                ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </>
  );
};

export default LoadList;
