import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Message from '../components/Message/Message';
import Loader from '../components/Loader/Loader';
import FormContainer from '../components/FormContainer/FormContainer';
import { addTruck, updateTruck } from '../actions/truckActions';

const TruckEditScreen = () => {
  const [type, setType] = useState('SPRINTER');
  
  let location = useLocation();
  const truck = location.state?.truck; 
  const dispatch = useDispatch();
    
  useEffect(()=>{
    if (truck) {
      setType(truck.type);
    }
     return () => {
       dispatch({type: 'RESET_MESSAGES'})
     }
  },[truck, dispatch])


  const truckState = useSelector((state) => state.trucks);
  const { loading, error, message } = truckState;

  const submitHandler = (e) => {
    e.preventDefault();
      if (truck) {
        // DISPATCH UPDATE TRUCK
        dispatch(updateTruck(truck._id, type));
      } else {
        // DISPATCH ADD TRUCK
        dispatch(addTruck(type));
      }
  };
  return (
    <FormContainer>
      <h1>{truck ? 'Edit truck' : 'Add Truck'}</h1>
      {error && <Message variant='danger'>{error}</Message>}
      {message && <Message variant='success'>{message}</Message>}
      {loading && <Loader/>}
      <Form onSubmit={submitHandler}>
      <Form.Group controlId="type">
          <Form.Label>Select truck type</Form.Label>
          <Form.Control as="select" onChange={(e) => setType(e.target.value)} value={type}>
            <option>SPRINTER</option>
            <option>SMALL STRAIGHT</option>
            <option>LARGE STRAIGHT</option>
          </Form.Control>
        </Form.Group>
        <Button type='submit' variant='primary'>Save</Button>
      </Form>
    </FormContainer>
  )
}

export default TruckEditScreen
