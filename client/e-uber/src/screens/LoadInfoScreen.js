import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getActiveLoad, changeLoadState } from '../actions/truckActions';
import { Table, Button, Row, Col } from 'react-bootstrap';
import Message from '../components/Message/Message';
import Loader from '../components/Loader/Loader';

const LoadInfoScreen = () => {
  const trucksState = useSelector((state) => state.trucks);
  const { loading, error, activeLoad, message } = trucksState;
  const dispatch = useDispatch();

  const stateHandler = () => {
    dispatch(changeLoadState());
  };

  useEffect(() => {
    dispatch(getActiveLoad());
  }, [dispatch]);
  return (
    <>
    <h1>Active Load Info</h1>
      {error && <Message variant="danger">{error}</Message>}
      {message && <Message variant="success">{message}</Message>}
      {loading && <Loader />}
      <Row>
        <Col>
          <Table striped bordered hover size="sm">
            {activeLoad && (
              <tbody>
                <tr>
                  <td>
                    <strong>Load Name</strong>
                  </td>
                  <td>{activeLoad.name}</td>
                </tr>
                <tr>
                  <td>
                    <strong>Pickup Address</strong>
                  </td>
                  <td>{activeLoad.pickup_address}</td>
                </tr>
                <tr>
                  <td>
                    <strong>Delivery Address</strong>
                  </td>
                  <td>{activeLoad.delivery_address}</td>
                </tr>
                <tr>
                  <td>
                    <strong>Payload</strong>
                  </td>
                  <td>{activeLoad.payload}</td>
                </tr>
                <tr>
                  <td>
                    <strong>Dimensions</strong>
                  </td>
                  <td>
                    Width: {activeLoad.dimensions.width}, Length:{' '}
                    {activeLoad.dimensions.length}, Height:{' '}
                    {activeLoad.dimensions.height}
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>State</strong>
                  </td>
                  <td>{activeLoad.state}</td>
                </tr>
              </tbody>
            )}
          </Table>
        </Col>
      </Row>
      <Button variant="primary" className="my-3" onClick={stateHandler}>
        Change state
      </Button>
    </>
  );
};

export default LoadInfoScreen;
