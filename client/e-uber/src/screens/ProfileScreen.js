import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Message from '../components/Message/Message';
import Loader from '../components/Loader/Loader';
import { getUserDetails, changeUserPassword, deleteProfile } from '../actions/userActions';

const ProfileScreen = ({ location, history }) => {
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');

  const dispatch = useDispatch();

  const userState = useSelector((state) => state.user);
  const { loading, error, userProfile, message } = userState;

  useEffect(() => {
    if (!userState.userInfo) {
      history.push('/login');
    } else {
      dispatch(getUserDetails());
    }
    return () => {
      dispatch({type: 'RESET_MESSAGES'})
    }
  }, [dispatch, history, userState.userInfo]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(changeUserPassword({ oldPassword, newPassword }));
  };

  const deleteHandler = () => {
    dispatch(deleteProfile());
  };

  return (
    <Row>
      <Col md={6} className="mx-auto">
        <h2>User Profile</h2>
        {error && <Message variant="danger">{error}</Message>}
        {message && <Message variant="success">{message}</Message>}
        {loading && <Loader />}
        <Form onSubmit={submitHandler}>
          <Form.Group controlId="email">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={userProfile?.user && userProfile.user.email}
              disabled
            ></Form.Control>
          </Form.Group>

          <Form.Group controlId="oldPassword">
            <Form.Label>Old Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter old password"
              value={oldPassword}
              onChange={(e) => setOldPassword(e.target.value)}
            ></Form.Control>
          </Form.Group>

          <Form.Group controlId="newPassword">
            <Form.Label>New Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter new password"
              value={newPassword}
              onChange={(e) => setNewPassword(e.target.value)}
            ></Form.Control>
          </Form.Group>
          <Row>
            <Col className="mx-auto">
              <Button type="submit" variant="primary">
                Update
              </Button>
              <Button variant="danger" className="ml-3" onClick={deleteHandler}>
                Delete Profile
              </Button>
            </Col>
          </Row>
        </Form>
      </Col>
    </Row>
  );
};

export default ProfileScreen;
