import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { getShippingInfo } from '../actions/loadActions';
import { Table, Row, Col } from 'react-bootstrap';
import Message from '../components/Message/Message';
import Loader from '../components/Loader/Loader';

const ShippingInfoScreen = () => {
  let location = useLocation();
  const load = location.state?.load; 

  const trucksState = useSelector((state) => state.loads);
  const { loading, error, shippingInfo } = trucksState;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getShippingInfo(load._id));
  }, [dispatch, load._id]);
  return (
    <>
    <h1>Shipping Info</h1>
      {error && <Message variant="danger">{error}</Message>}
      {loading && <Loader />}
    <h3>Load Info</h3>
      <Row>
        <Col>
          <Table striped bordered hover size="sm">
            {load && (
              <tbody>
                <tr>
                  <td>
                    <strong>Load Name</strong>
                  </td>
                  <td>{load.name}</td>
                </tr>
                <tr>
                  <td>
                    <strong>Pickup Address</strong>
                  </td>
                  <td>{load.pickup_address}</td>
                </tr>
                <tr>
                  <td>
                    <strong>Delivery Address</strong>
                  </td>
                  <td>{load.delivery_address}</td>
                </tr>
                <tr>
                  <td>
                    <strong>Payload</strong>
                  </td>
                  <td>{load.payload}</td>
                </tr>
                <tr>
                  <td>
                    <strong>Dimensions</strong>
                  </td>
                  <td>
                    Width: {load.dimensions.width}, Length:{' '}
                    {load.dimensions.length}, Height:{' '}
                    {load.dimensions.height}
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>State</strong>
                  </td>
                  <td>{load.state}</td>
                </tr>
              </tbody>
            )}
          </Table>
        </Col>
      </Row>
    <h3>Truck Info</h3>
      <Row>
        <Col>
          <Table striped bordered hover size="sm">
            {shippingInfo && (
              <tbody>
                <tr>
                  <td>
                    <strong>Truck type</strong>
                  </td>
                  <td>{shippingInfo.truck.type}</td>
                </tr>
              </tbody>
            )}
          </Table>
        </Col>
      </Row>
    </>
  );
};

export default ShippingInfoScreen;
