import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Table, Button, Row, Col } from 'react-bootstrap';
import Message from '../components/Message/Message';
import Loader from '../components/Loader/Loader';
import { getTruckList } from '../actions/truckActions';
import TruckItem from '../components/TruckItem/TruckItem';

const TruckList = ({history}) => {
  const truckState = useSelector((state) => state.trucks);
  const { loading, error, message, trucks } = truckState;
  const dispatch = useDispatch();

  const userState = useSelector((state) => state.user);

  useEffect(() => {
    if (!userState.userInfo) {
      history.push('/login');
    } else {
      dispatch(getTruckList());
    }
    return () => {
      dispatch({type: 'RESET_MESSAGES'})
    }
  }, [dispatch, history, userState.userInfo]);
  return (
    <>
      <h1>Truck List</h1>
      {error && <Message variant="danger">{error}</Message>}
      {message && <Message variant="success">{message}</Message>}
      {loading && <Loader />}

      <Link to='/trucks/add'>
        <Button variant="primary" className="my-3">Add truck</Button>
      </Link>
      <Row>
        <Col>
          <Table striped bordered hover size="sm">
            <thead>
              <tr style={{'textAlign':'center'}}>
                <th>#</th>
                <th>Truck type</th>
                <th>Assign</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {trucks &&
                trucks.map((truck, index) => (
                  <TruckItem truck={truck} index={index} key={truck._id} />
                ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </>
  );
};

export default TruckList;
