import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { logout } from '../../actions/userActions';

const Header = () => {
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.user.userInfo);

  const logoutHandler = () => {
    dispatch(logout());
  }
  return (
    <header>
      <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect>
        <Container>
          <LinkContainer to="/">
            <Navbar.Brand>E-UBER</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <LinkContainer to="/trucks">
                  <Nav.Link>
                    TRUCKS
                  </Nav.Link>
                </LinkContainer>
                <LinkContainer to="/loads">
                  <Nav.Link>
                    LOADS
                  </Nav.Link>
                </LinkContainer>
            </Nav>
            <Nav className="ml-auto">
              {userInfo ? (
                <>
                <LinkContainer to="/profile">
                  <Nav.Link>
                    <i className="fas fa-user mr-1"></i>Profile
                  </Nav.Link>
                </LinkContainer>
                  <Nav.Link onClick={logoutHandler}>Logout</Nav.Link>
                  </>
              ) : (
                <LinkContainer to="/login">
                  <Nav.Link>
                    <i className="fas fa-user mr-1"></i>Sign In
                  </Nav.Link>
                </LinkContainer>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
