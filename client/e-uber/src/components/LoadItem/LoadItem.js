import React from 'react';
import { useDispatch } from 'react-redux';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { deleteLoad, postLoad } from '../../actions/loadActions';

const LoadItem = ({ load, index }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const deleteHandler = () => {
    dispatch(deleteLoad(load));
  };

  const editHandler = () => {
    history.push({
      pathname: '/loads/add',
      state: { load }
    });
  };

  const shippingInfoHandler = () => {
    history.push({
      pathname: '/loads/info',
      state: { load }
    });
  };

  const postHandler = () => {
    dispatch(postLoad(load));
  };
  return (
    <tr style={{ textAlign: 'center' }}>
      <td>{index + 1}</td>
      <td>
        {load.name}
        {load.status === 'ASSIGNED' && (
          <Button variant="info" size="sm" onClick={shippingInfoHandler}>
            INFO
          </Button>
        )}
      </td>
      <td>{load.status}</td>
      <td>
        <Button variant="primary" size="sm" onClick={postHandler}>
          <i className="fas fa-shipping-fast fa-2x"></i>
        </Button>
      </td>
      <td>
        <Button variant="primary" size="sm" onClick={editHandler}>
          <i className="fas fa-edit fa-2x"></i>
        </Button>
      </td>
      <td>
        <Button variant="primary" size="sm" onClick={deleteHandler}>
          <i className="fas fa-trash-alt fa-2x"></i>
        </Button>
      </td>
    </tr>
  );
};

export default LoadItem;
