import React from 'react';
import { useDispatch } from 'react-redux';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { deleteTruck, assignTruck } from '../../actions/truckActions';

const TruckItem = ({ truck, index }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const deleteHandler = () => {
    dispatch(deleteTruck(truck));
  };

  const editHandler = () => {
    history.push({
      pathname: '/trucks/add',
      state: { truck }
    });
  };

  const loadInfoHandler = () => {
    history.push({
      pathname: '/trucks/load/active',
    });
  };

  const assignHandler = () => {
    dispatch(assignTruck(truck));
  };
  return (
    <tr style={{ textAlign: 'center' }}>
      <td>{index + 1}</td>
      <td>
        {truck.type}
        {truck.assigned_to ? ' (Assigned)  ' : ''}
        {truck.status === 'OL' && (
          <Button variant="info" size="sm" onClick={loadInfoHandler}>
            LOAD
          </Button>
        )}
      </td>
      <td>
        <Button variant="primary" size="sm" onClick={assignHandler}>
          <i className="fas fa-user-plus fa-2x"></i>
        </Button>
      </td>
      <td>
        <Button variant="primary" size="sm" onClick={editHandler}>
          <i className="fas fa-edit fa-2x"></i>
        </Button>
      </td>
      <td>
        <Button variant="primary" size="sm" onClick={deleteHandler}>
          <i className="fas fa-trash-alt fa-2x"></i>
        </Button>
      </td>
    </tr>
  );
};

export default TruckItem;
