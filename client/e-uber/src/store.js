import {createStore, combineReducers, applyMiddleware} from 'redux';
import  thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { loadReducer } from './reducers/loadReducer';
import { truckReducer } from './reducers/truckReducer';
import { userReducer } from './reducers/userReducers';

const reducer = combineReducers({
  trucks:  truckReducer,
  loads: loadReducer,
  user: userReducer,
});

const userInfoFromStorage = localStorage.getItem('userInfo') 
? JSON.parse(localStorage.getItem('userInfo')) : null;

const initialState = {
  user: {userInfo: userInfoFromStorage, userProfile:{}}
};

const middleware = [thunk];

const store = createStore(reducer, initialState, composeWithDevTools(
  applyMiddleware(...middleware)
));

export default store;