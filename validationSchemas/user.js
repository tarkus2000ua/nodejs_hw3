const joi = require('joi');

module.exports.changePasswordSchema = joi.object({
  oldPassword: joi.string()
      .required(),
  newPassword: joi.string()
      // .invalid(joi.ref('oldPassword'))
      .required(),
});
