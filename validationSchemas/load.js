const joi = require('joi');

module.exports.addLoadSchema = joi.object({
  name: joi.string()
      .required(),
  pickup_address: joi.string()
      .required(),
  delivery_address: joi.string()
      .required(),
  dimensions: {
    width: joi.number()
        .required(),
    length: joi.number()
        .required(),
    height: joi.number()
        .required(),
  },
  payload: joi.number()
      .required(),
});
