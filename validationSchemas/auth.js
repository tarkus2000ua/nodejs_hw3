const joi = require('joi');

module.exports.registerSchema = joi.object({
  email: joi.string()
      .email()
      .required(),
  password: joi.string()
      .required(),
  role: joi.string()
      .valid('DRIVER', 'SHIPPER')
      .required(),
});

module.exports.loginSchema = joi.object({
  email: joi.string()
      .email()
      .required(),
  password: joi.string()
      .required(),
});
