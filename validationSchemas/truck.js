const joi = require('joi');

module.exports.addTruckSchema = joi.object({
  type: joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required(),
});
