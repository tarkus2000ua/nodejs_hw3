const {
  Router,
} = require('express');
// eslint-disable-next-line new-cap
const router = Router();

const authMiddleware = require('../middleware/authMiddleware');
const {
  allowShipper,
  allowDriver,
} = require('../middleware/roleMiddleware');

const {
  getLoads,
  getLoad,
  getActiveLoad,
  addLoad,
  postLoad,
  updateLoadInfo,
  updateLoadState,
  deleteLoad,
  getShippingInfo,
} = require('../controllers/loadsController');

router.get('/active', authMiddleware, allowDriver, getActiveLoad);
router.get('/', authMiddleware, getLoads);
router.get('/:loadId', authMiddleware, getLoad);
router.post('/', authMiddleware, allowShipper, addLoad);
router.delete('/:loadId', authMiddleware, allowShipper, deleteLoad);
router.post('/:loadId/post', authMiddleware, allowShipper, postLoad);
router.patch('/active/state', authMiddleware, allowDriver, updateLoadState);
router.put('/:loadId', authMiddleware, allowShipper, updateLoadInfo);
router.get('/:loadId/shipping_info', authMiddleware, allowShipper,
    getShippingInfo);

module.exports = router;
