const {
  Router,
} = require('express');
// eslint-disable-next-line new-cap
const router = Router();

const authMiddleware = require('../middleware/authMiddleware');
const {allowDriver} = require('../middleware/roleMiddleware');

const {
  getTrucks,
  addTruck,
  getTruckInfo,
  assignTruck,
  updateTruckInfo,
  deleteTruck,
} = require('../controllers/trucksController');

router.get('/', authMiddleware, allowDriver, getTrucks);
router.post('/', authMiddleware, allowDriver, addTruck);
router.get('/:truckId', authMiddleware, allowDriver, getTruckInfo);
router.delete('/:truckId', authMiddleware, allowDriver, deleteTruck);
router.post('/:truckId/assign', authMiddleware, allowDriver, assignTruck);
router.put('/:truckId', authMiddleware, allowDriver, updateTruckInfo);

module.exports = router;
