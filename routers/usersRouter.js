const {
  Router,
} = require('express');
// eslint-disable-next-line new-cap
const router = Router();

const authMiddleware = require('../middleware/authMiddleware');

const {
  getUserProfile,
  deleteUserProfile,
  changeUserPassword,
} = require('../controllers/usersController');

router.get('/', authMiddleware, getUserProfile);
router.delete('/', authMiddleware, deleteUserProfile);
router.patch('/password', authMiddleware, changeUserPassword);

module.exports = router;
