const authRouter = require('./authRouter');
const usersRouter = require('./usersRouter');
const trucksRouter = require('./trucksRouter');
const loadsRouter = require('./loadsRouter');

module.exports = (app) => {
  app.use('/api/auth', authRouter);
  app.use('/api/users/me', usersRouter);
  app.use('/api/trucks', trucksRouter);
  app.use('/api/loads', loadsRouter);
};
