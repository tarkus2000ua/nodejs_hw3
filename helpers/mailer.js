const nodemailer = require('nodemailer');
const asyncHandler = require('express-async-handler');
const config = require('config');

const smtpConfig = config.get('smtp');

module.exports.sendEmail = asyncHandler(async (text, email) => {
  const transporter = nodemailer.createTransport({
    host: smtpConfig.host,
    port: smtpConfig.port,
    secure: smtpConfig.secure,
    auth: {
      user: smtpConfig.user,
      pass: smtpConfig.password,
    },
  });

  const info = await transporter.sendMail({
    from: `"E-Uber" <${smtpConfig.user}>`,
    to: email,
    subject: 'E-Uber password reset',
    text: text,
  });

  console.log('Message sent: %s', info.messageId);

  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

  return 'success';
});
