**NODE.JS Homework #3**

To install project dependencies run:

    npm install

To start server run command:

    npm start
To start both server and client run:

    npm run dev
Default server port is 5000. If you want to use another port you should change next line in package.json file inside the "client" folder accordingly:

    "proxy": "http://127.0.0.1:5000",

Default client port is 3000

Server API documentation will be available at:
 http://serverAddress:port/docs