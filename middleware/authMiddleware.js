const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const asyncHandler = require('express-async-handler');
const config = require('config');

const authConfig = config.get('auth');

const {
  secret,
} = authConfig;

module.exports = asyncHandler(async (req, res, next) => {
  const authHeader = req.headers['authorization'];

  if (!authHeader) {
    throw createError(401, 'No authorization header found');
  }

  const [, jwtToken] = authHeader.split(' ');

  req.user = await jwt.verify(jwtToken, secret);
  if (!req.user) throw createError(401, 'Invalid JWT');
  next();
});
