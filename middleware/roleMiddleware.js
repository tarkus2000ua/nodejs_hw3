const createError = require('http-errors');

module.exports.allowDriver = (req, res, next) => {
  const {role} = req.user;
  if (role !== 'DRIVER') {
    throw createError(401, 'Access denied');
  }
  next();
};

module.exports.allowShipper = (req, res, next) => {
  const {role} = req.user;
  if (role !== 'SHIPPER') {
    throw createError(401, 'Access denied');
  }
  next();
};
